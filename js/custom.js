var h = jQuery("#header-sroll");
jQuery(window).scroll(function () {
    var windowpos = jQuery(window).scrollTop();
    if (windowpos >= 200) {
        h.addClass("small");
    } else {
        h.removeClass("small");
    }
});
jQuery(document).on('click', '.menu-item', function (e) {
    e.preventDefault();
    jQuery('li').removeClass('current-menu-item');
    jQuery(this).addClass('current-menu-item');
    var index = jQuery(this).find('a').text();
    var headerClass = jQuery('#header-sroll').hasClass('small');
    index = (index == "Benefits of AV" ? 'BenefitsofAV' : index);
    index = (index == "Effective Uses of AV" ? 'EffectiveUsesofAV' : index);
    index = (index == "Businesses Served" ? 'BusinessesServed' : index);
    index = (index == "Free Consultation" ? 'FreeConsultation' : index);
    
    if (jQuery('#' + index).length > 0) {
        if (headerClass) {
            jQuery('html,body').animate({
                scrollTop: jQuery('#' + index).offset().top - 73
            }, 2500);
        } else {
            if (jQuery(window).width() < 767) {
                jQuery('html,body').animate({
                    scrollTop: jQuery('#' + index).offset().top - 175
                }, 2500);
            } else {
                jQuery('html,body').animate({
                    scrollTop: jQuery('#' + index).offset().top - 150
                }, 2500);
            }
        }
    }
});

function mobileElements() {
  if (jQuery(window).width() < 991) {
    jQuery(".navbar-nav li.menu-item a").attr({
      "data-toggle": "collapse",
      "data-target": "#collapsibleNavbar"
    });
  }
    else if (jQuery(window).width() > 991) {
       jQuery(".navbar-nav li.menu-item a").attr({
      "data-toggle": " ",
      "data-target": " "
    }); 
    }

};


mobileElements();

jQuery(window).on("load resize", function() {
  mobileElements();
});

